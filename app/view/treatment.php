<div id="content">
	<div class="row">
			<article class="column">
					<h1>Eco-Friendly Water Treatment Systems</h1><br />
					<div class="left">
						<p>Get rid of limestone residue from your water with our practical water treatment systems. Located in Pflugerville, Texas, Choice Water Conditioning, LLC provides quality filtration products and prompt salt delivery.</p>
						<h2>Treat Your Water</h2>
						<p>Purify your water to get rid of any mineral impurity, which may leave troublesome residues on dishes and fixtures. Hard water also causes all water-using appliances in your home to work less efficiently. Lastly, it is a major cause of dry skin and hair in people with sensitive skin. We primarily deal in Puritan metered units that measure the water as you use it, meaning there's no need to mess with any timers! Click any one of the following links to learn more about what we offer.</p>
						<ul>
								<li>To learn about the Puritan CL30 Digital Metered Water Softener, <a href="#">click here</a></li>
								<li>To learn about the Puritan L30 Mechanical Metered Water Softener, <a href="#">click here</a></li>
								<li>To learn what other systems we service and can provide, <a href="#">click here</a> or leave us a message</li>
						</ul>
					</div>
					<div class="right">
							<img src="public/images/content/trt1.png" alt="" />
					</div>
					<div class="left">
						<h2>Water Testing</h2>
						<p>Have you ever wondered what you and your family are drinking? We offer free water testing to help assess exactly what your water treatment needs are.</p>

						<h2>About Puritan</h2>
						<p>Puritan Water Conditioning has been providing water filtration products since 1905. Made in the USA in Crawfordsville, Indiana, Puritan offers a full line of water treatment products.</p>
					</div>
					<div class="right">
						<center><img src="public/images/content/puritan-logo.png" alt=""/></center>
					</div>
					<center><p><strong>Contact us in Pflugerville, Texas, for safe and effective water treatment systems.</strong></p></center>
					<h1>Puritan Digital Metered Water Softener</h1><br />
					<div class="left ">
						<h2>*CL30 Model Pictured*</h2>
						<p>The CL30 Puritan Digital Metered Water Softener system is designed for those with total household control in mind. While being one of the most efficient and resilient systems on the market, the CL30 offers additional features that allow the user to perform a whole number of tasks as well as provide the user with invaluable measurements and information. Underneath the advanced features lies the same reliable water softening technology present in all of Puritan’s durable softeners, meaning that you can rest assured that your system will be able to stand up to even the most demanding of situations. Ask about our package specials! For more information, give us a call, or fill out our website information request form. We at Choice Water Conditioning are always happy to assist!</p>
						<h2>How does the CL30 compare to the L30?</h2>
						<p>The CL30 was designed with the ultimate level of utility in mind. It offers an unprecedented level of control over your water softener and is the most state of the art system we offer. When compared to the L30, it is easy to see that the CL30 offers far more features than the L30, but keep in mind that the L30 was designed with cost efficiency in mind. At Choice Water Conditioning, we want you to have the best system for your situation. Here are a few points to compare on the two systems.</p>
					</div>
					<div class="right">
					 	<img src="public/images/content/puritan.jpg" alt="" />
					</div>
					<div class="left">
						<div class="span5 column">
						<h2>Warranty Information</h2>
						<p>All tanks are warrantied for life by Puritan. All control heads, both mechanical and digital, are covered by a five-year warranty. All installations completed by Choice Water Conditioning, LLC are also covered by a one year warranty on labor.</p>
						</div>
						<h2>Carbon Filter Package Option</h2>
						<p>This system qualifies for a package discount on our top of the line whole house carbon filtration system! Be sure to ask us how you can take advantage of this deal so you can start enjoying the benefit of soft, carbon filtered water today!</p>
					</div>
					<div class="right">
						<table class="trt">
								<tr>
										<td></td>
										<th>CL30</th>
										<th>L30</th>
								</tr>
								<tr>
										<td>Perfectly Softened Water</td>
										<td>Yes!</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>Whole House Carbon Filtration</td>
										<td>Optional</td>
										<td>Optional</td>
								</tr>
								<tr>
										<td>Lifetime Warranty on Tanks</td>
										<td>Yes!</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>5 Year Warranty on Control Head</td>
										<td>Yes!</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>1 Year Warranty on Labor</td>
										<td>Yes!</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>Water Flow Indicator</td>
										<td>Yes!</td>
										<td>No</td>
								</tr>
								<tr>
										<td>8 Hour Back Up Battery</td>
										<td>Yes!</td>
										<td>No</td>
								</tr>
								<tr>
										<td>Up to 18 Gallons per Minute Flow Rate</td>
										<td>Yes!</td>
										<td>No</td>
								</tr>
						</table>
					</div>
					<h1>Puritan Mechanical Metered Water Softener</h1>
					<div class="left">
						<h2>*L30 Model Pictured*</h2>
						<p>The L30 Puritan Mechanical Metered Water Softener system is a highly versatile water softener designed to handle the most demanding of situations from shower-singing spouses and children to added water consumption by family members during the holidays. With a powerfully accurate metered control head, the 5600 series is highly efficient at managing resources without sacrificing utility. Designed with cost efficiency in mind, this system delivers the most ‘bang for your buck.’ As a bonus, this system can be packaged with an optional whole house carbon filtration system! For more information, give us a call, or fill out our website information request form. We at Choice Water Conditioning are always happy to assist!</p>
						<h2>How does the L30 compare to the CL30?</h2>
						<p>Both systems will deliver the exact same high quality of water for you to enjoy! They are both covered by the same warranties and are generally capable of the same basic features. However, the L30 is geared towards cost efficiency and the CL30 is geared towards adding a whole new level of functionality. Check out the table below to see a just a few of the differences!</p>
					</div>
					<div class="right">
							<img src="public/images/content/puritan2.jpg" alt="" />
					</div>
					<div class="left">
						<div class="span5 column">
						<h2>Warranty Information</h2>
						<p>All tanks are warrantied for life by Puritan. All control heads, both mechanical and digital, are covered by a five-year warranty. All installations completed by Choice Water Conditioning, LLC are also covered by a one year warranty on labor.</p>
						</div>
						<div class="span5 columnr">
						<h2>Carbon Filter Package Option</h2>
						<p>This system qualifies for a package discount on our top of the line whole house carbon filtration system! Be sure to ask us how you can take advantage of this deal so you can start enjoying the benefit of soft, carbon filtered water today!</p>
						</div>
					</div>
					<div class="right">
						<table class="trt">
								<tr>
										<td></td>
										<th>L30</th>
										<th>CL30</th>
								</tr>
								<tr>
										<td>Perfectly Softened Water</td>
										<td>Yes!</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>Whole House Carbon Filter</td>
										<td>Optional</td>
										<td>Optional</td>
								</tr>
								<tr>
										<td>Lifetime Warranty on Tanks</td>
										<td>Yes!</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>5 Year Warranty on Control Head</td>
										<td>Yes!</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>1 Year Warranty on Labor</td>
										<td>Yes!</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>Water Flow Indicator</td>
										<td>No</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>8 Hour Backup Batter</td>
										<td>No</td>
										<td>Yes!</td>
								</tr>
								<tr>
										<td>Up to 18 Gallons per Minute Flow Rate</td>
										<td>No</td>
										<td>Yes!</td>
								</tr>
						</table>
					</div>
					<h1>Other System</h1>
					 <h2>Your options are never limited!</h2>
					 <p>While the metered softener systems are our main "bread and butter" systems, we do offer timered units as well. We are also able to get our hands on any number of systems you may want. All you need to do is ask, and we will talk to you about what would work best for you and your situation. Keep in mind that, at Choice Water Conditioning, we highly recommend the Puritan systems. We all personally have these systems in our homes and Puritan's durability and efficiency has really stood the test of time. If you are interested in learning more about our system offerings, as well as what we can get for you, give us a call or send us a message on our contact form! We are always happy to help!</p>
					 <h2>Yes, we service Fleck and Clack systems, too!</h2>
					 <p>If you ever have any questions about what we can work on, give us a call or leave us a message and we will gladly help you out!</p>
					<br class="clear">
			</article>
			<?php //include('includes/sidebar.php');?>
			<br class="clear">
	</div>
</div>
