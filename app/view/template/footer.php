<footer>
	<div id="footer">
		<div class="row">
			<div class="navTop">
				<div class="cols col-1">
					<h2>GET IN TOUCH WITH US</h2>
					<img src="public/images/content/footer-logo.png" alt="Footer Logo">
				</div>
				<div class="cols col-2">
					<p>Phone:<span><?php $this->info(["phone","tel"]);?></span></p>
					<p>Email:<span><?php $this->info(["email","mailto"]);?></span></p>
					<p>Address:<span><?php $this->info("address");?></span></p>
				</div>
				<div class="cols col-3">
					<h3>BUSINESS HOURS</h3>
					<ul>
						<li><span>MONDAY</span>8:00 AM - 4:00 PM</li>
						<li><span>TUESDAY</span>8:00 AM - 4:00 PM</li>
						<li><span>WEDNESDAY</span>8:00 AM - 4:00 PM</li>
						<li><span>THURSDAY</span>8:00 AM - 4:00 PM</li>
						<li><span>FRIDAY</span>8:00 AM - 4:00 PM</li>
						<li><span>SATURDAY</span>CLOSED</li>
						<li><span>SUNDAY</span>CLOSED</li>
					</ul>
				</div>
				<div class="cols col-4">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3440.026207563603!2d-97.56565068436413!3d30.435358981741913!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644c44df7d61457%3A0x725c178cfe50e7d7!2s6107+Jesse+Bohls+Dr%2C+Pflugerville%2C+TX+78660%2C+USA!5e0!3m2!1sen!2sph!4v1531304839258" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Google Maps iframe generator</a></iframe><br />
				</div>
			</div>
			<div class="navBot">
				<div class="cols col-1">
					<p>WE ACCEPT</p>
					<img src="public/images/content/visa.png" alt="Visa Card">
					<img src="public/images/content/discover.png" alt="Discover Card">
					<img src="public/images/content/american.png" alt="American Express Card">
					<img src="public/images/content/master.png" alt="MasterCard">
				</div>
				<div class="cols col-2">
					<div class="navBar-footer">
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("treatment"); ?>><a href="<?php echo URL ?>treatment#content">TREATMENT</a></li>
								<li <?php $this->helpers->isActiveMenu("filters"); ?>><a href="<?php echo URL ?>filters#content">FILTERS</a></li>
								<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">ABOUT US</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
							</ul>
					</div>
					<p class="copy">
						© <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?> ALL RIGHTS RESERVERD.
						<?php if( $this->siteInfo['policy_link'] ): ?>
							<a href="<?php $this->info("policy_link"); ?>">PRIVACY POLICY</a>.
						<?php endif ?>
					</p>
					<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank"> Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
				</div>
				<div class="cols col-3">
					<p>FOLLOW US</p>
					<p class="socFooter">
	                	<a href="<?php $this->info("fb_link"); ?>" target="_blank">f</a>
						<a href="<?php $this->info("gp_link"); ?>" target="_blank">g</a>
						<a href="<?php $this->info("li_link"); ?>" target="_blank">i</a>
						<a href="<?php $this->info("tb_link"); ?>" target="_blank">t</a>
						<a href="<?php $this->info("tt_link"); ?>" target="_blank">l</a>
	                </p>
				</div>
			</div>
		</div>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			});
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 5, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
