<div id="content">
	<div class="row">
		<article class="column">
				<h1>ABOUT US</h1>
				<p>Choice Water Conditioning, LLC in Pflugerville, Texas, wants every home and business owner to take advantage of our water filtration systems. As a locally-owned business, we are committed to providing our customers with top-performing products at the most competitive prices.</p>

				<h2>Meet the Founder</h2>

				<p>Greg Gould, our owner, has been in the water business since 1990. Prior to getting into water conditioning business, Greg did construction inspection for Pflugerville—where he has lived since 1989. Greg likes to say that "he helped build Pflugerville," after seeing and experiencing the tremendous growth in the city.</p>


				<h2>Pursuing the Dream</h2>

				<p>Greg always aspired to manage his own business, since he has grown up around his Dad who had a successful real estate business in Galveston Island. Our founder was once named #1 volume producer in Texas, and #2 volume producer in the nation, while working for another water conditioning company. After nine years in the water conditioning field, our owner was urged and convinced by colleagues to go out on his own. Greg pursued his dream and he's now a Certified Water Treatment Specialist, so you know you are in good hands.</p>


				<h2>Satisfaction Guaranteed</h2>

				<p>At Choice Water Conditioning, LLC, we only provide top-quality products and services. Unlike other companies that try to sell you their home brand, we provide products and services based on your needs. We have a little overhead, which allows us to pass the savings directly to you. If you would like to hear what others are saying, be sure to check out our profile on <a href="http://www.angieslist.com/companylist/us/tx/pflugerville/choice-water-conditioning-llc-reviews-284090.htm" target="_blank">Angie's List</a> or  <a href="http://yelp.com/biz/choice-water-conditioning-pflugerville?utm_medium=badge_small&amp;utm_source=biz_review_badge" target="_blank">Yelp</a>.
<!-- or the <a href="http://www.bbb.org/central-texas/business-reviews/water-softening-conditioning-equipment-service-supplies/choice-water-conditioning-llc-in-pflugerville-tx-48487/" target="_blank">BBB</a>. --></p>


				<h2>Going the Extra Mile</h2>

				<p>Since our goal is to guarantee your satisfaction, we founded our company based on honesty and integrity. We don't have a one-size-fits-all mentality. Our skilled installers service a wide variety of makes and models.</p>

				<h3>REVIEWS</h3>
				<h2 class="rev">Review 1:</h2>
				<p class="p-rev">"I have used this good ol' boy team for a few years now with several clients (I'm a contractor). Every time they never cease to amaze me by finding the little itty bitty problem that is making the water softener system not work correctly. Each fix is usually very reasonable in price and they always show you what is going on. I consider them honest without fluff." <strong>- Tracy H.</strong></p>

				<h2 class="rev">Review 2:</h2>
				<p class="p-rev">"I had my water softener go out on me, so I called Choice Water to come take a look as it was purchased through them by the original owner.  Greg came out right on time (I mean how often does this happen!) to take a look.</p>
				<p>When he got there, everything seemed to check out.  We did a manual regeneration of the tank and changed out the carbon tank.  Since there was nothing going on with my system, he didn't even charge me the $100 service call fee!  The total ended up being less than originally quoted, and Greg was in and out within less than 30 minutes.</p>
				<p class="p-rev">I fully recommend Choice Water for new water softener systems AND for service calls!" <strong>- Sarah N.</strong></p>

				<h2 class="rev">Review 3:</h2>
				<p class="p-rev">"My family currently uses Culligan, and overall it's been a good experience. However, we're ready to move from renting to owning but Culligan won't let us buy our rent unit and their owned units are over $400 more than others we've reviewed. We had Greg Gould from Choice Water Conditioning come out and he really connected with us. His presentation to our needs was effective - not competitor-bashing - and he even came with a printed list of nearby residents using his service; some of which we know as friends. We're now his new customer! Greg did an excellent job of listening, presenting, and helping us feel comfortable with our investment. If your family is looking for a water softener then be sure to consider the Puritan product line and the folks at Choice Water Conditioning." <strong>- Eric T.</strong></p>
		</article>
	</div>
</div>
