<div id="content">
	<div class="row">
		<div class="cont-left">
			<p class="welcome">Welcome To</p>
			<h1>CHOICE<span>WATER CONDITIONING, LLC</span></h1>
			<p >Choice Water Conditioning, LLC offers energy-saving water filtration systems in Pflugerville, Texas. With more than 20 years of experience, our family-owned business provides water treatment systems, which include reverse osmosis and whole house carbon filters. We pride ourselves in servicing a wide variety of makes and models. In addition to product offerings, our company also provides salt delivery and reminder calls for filter changes. If we compare apples to apples, you’ll see that we truly help you save money with our state of the art filtration and treatment systems.</p>
			<p>We provide quality water filtration products and top notch service. We supply products to meet your needs. In this area, naturally occurring hard water is common. This can cause spots on dishes, glasses, and shower doors, as well as dry out your skin and cause soaps not to lather as well. Hard water also causes water using appliances to wear out faster. Our water softeners can provide the answer.</p>
			<div class="button">
				<a href="about#content">Read More</a>
			</div>
			<div class="call">
				<p class="callus">CALL US TODAY!<span><?php $this->info(["phone","tel"]);?></span></p>
			</div>
		</div>
		<div class="contImage">
			<img src="public/images/content/cont-img.jpg" alt="Content Image">
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<h2>Our Services</h2>
		<dl>
			<dt><img src="public/images/content/service-img1.png" alt="Service Image 1"></dt>
			<dd>
				<h3>Water Softener</h3>
				<p>Improve your water with water treatment systems from Choice Water Conditioning, LLC. We offer top-quality filtration systems at the most competitive prices.</p>
			</dd>
		</dl>
		<dl>
			<dt><img src="public/images/content/service-img2.png" alt="Service Image 2"></dt>
			<dd>
				<h3>Whole House Carbon Filters</h3>
				<p>Installing our whole house carbon filtration system along with the water softener removes chlorine and some organics, polishes the taste of the water and protects the life of the resin in the softener.</p>
			</dd>
		</dl>
		<dl>
			<dt><img src="public/images/content/service-img3.png" alt="Service Image 3"></dt>
			<dd>
				<h3>Drinking Water Systems</h3>
				<p>Let us install our reverse osmosis system for the most thorough purification for your drinking water. It is capable of filtering out 95% of water impurities and in some cases we are able to connect the reverse osmosis system to your fridge for ice and chilled water.</p>
			</dd>
		</dl>
		<dl>
			<dt><img src="public/images/content/service-img4.png" alt="Service Image 4"></dt>
			<dd>
				<h3>Service and Rentals</h3>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
			</dd>
		</dl>
	</div>
</div>
<div id="testimonials">
	<div class="row">
		<div class="test-right">
			<h2>Testimonials</h2>
			<h3>&#9733;&#9733;&#9733;&#9733;&#9733; - <span>Sarah N.</span></h3>
			<p>I had my water softener go out on me, so I called Choice Water to come take a look as it was purchased through them by the original owner.  Greg came out right on time (I mean how often does this happen!) to take a look.</p>
			<p>When he got there, everything seemed to check out.  We did a manual regeneration of the tank and changed out the carbon tank.  Since there was nothing going on with my system, he didn’t even charge me the $100 service call fee!  The total ended up being less than originally quoted, and Greg was in and out within less than 30 minutes. I fully recommend Choice Water for new water softener systems AND for service calls!</p>
			<a href="#">Read More</a>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<h2>Contact Us</h2>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<label><span class="ctc-hide">Name</span>
				<input type="text" name="name" placeholder="Name:">
			</label>
			<label><span class="ctc-hide">Phone</span>
				<input type="text" name="phone" placeholder="Phone:">
			</label>
			<label><span class="ctc-hide">Email</span>
				<input type="text" name="email" placeholder="Email:">
			</label>
			<label><span class="ctc-hide">Message</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message / Questions:"></textarea>
			</label>
			<!-- <label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label> -->
			<label>
				<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
			</label><br>
			<?php if( $this->siteInfo['policy_link'] ): ?>
			<label>
				<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
			</label>
			<?php endif ?>
			<label><span class="ctc-hide">Recaptcha</span></label>
			<div class="g-recaptcha fr"></div>
			<div class="clearfix"></div>
			<button type="submit" class="ctcBtn" disabled>SUBMIT FORM</button>
		</form>
	</div>
</div>
