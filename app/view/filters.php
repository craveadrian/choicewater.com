<div id="content">
	<div class="row">
		<article class="column">
			<h1>High-Tech Reverse Osmosis Filter and Carbon Filter</h1>
			<div class="left">
				<p>Get purified water directly from your faucet with products from Choice Water Conditioning, LLC. Based in Pflugerville, Texas, we provide money-saving reverse osmosis filter and carbon filter systems.</p>
				<h2>Reverse Osmosis and Whole House Carbon Filter</h2>
				<p>Let us install our reverse osmosis drinking system and whole house carbon filter for easy home water filtration. Many customers prefer reverse osmosis because it is capable of filtering out 95% of water impurities. However, whole house carbon filters are also a great way of improving your drinking water throughout the house, and are more suited for city water.</p>
			</div>
			<div class="right">
					<img src="public/images/content/filter.png" alt="" />
			</div>

				<center><p><strong>Contact us in Pflugerville, Texas, to order our high-performance reverse osmosis filter and water carbon filter.</strong></p></center>
				<h1>Whole House Carbon Filtration</h1>
				<p>Ever notice how the water in the area tends to taste a little less than pleasant? That is because the city adds chlorine to your water to make sure it kills bacteria before you drink the water. However, the chlorine remains in the water during this process. Our carbon filtration system is an entire tank dedicated to removing chlorine, polishing up the taste, and removing the chlorine odor at every source of water in your home. The water has to travel through a 3 foot carbon tank, maximizing contact time and filtration. On top of that, we install the carbon tank so the water gets filtered before it enters the softener. We do this to protect the life of the resin bed in the softener, as chlorine is the only thing that can damage the resin. Also, with this system in place, you no longer need to replace carbon filters on your refrigerator, nor will you need to continue buying those expensive bottled waters! Here are some of the benefits of a whole house carbon filter:</p>
				<ul class="filter">
						<li>Delicious, carbon filtered water at every source of water in your home!</li>
						<li>Eliminates the need of replacing refrigerator filters!</li>
						<li>Stops your clothes from fading in the laundry!</li>
						<li>Protects the life of the resin bed in your softener!</li>
				</ul>
				<p>As you can see, our Whole House Carbon Filtration system can greatly benefit you! Coupled with our water softener (did we mention that you can get the carbon filter discounted with a softener?), you will be well on your way to loving the water in your home today! Not to mention, protecting your water using appliances as well as the softener itself. Contact us today to learn how you can start loving the water in your home!</p>
				<div class="left">
					<h1>Reverse Osmosis System</h1>
					<p>Our reverse osmosis system is one of the most advanced and thorough systems on the market today. Utlizing a three stage filter, this system removes virtually all particulates, leaving you with highly filtered, very pure water that is excellent for coffee makers, aquariums and food preparation. The purchase of a Reverse Osmosis system through Choice Water Conditioning includes the filter, a holding tank, and an attractive faucet to be installed next at the sink over the unit. For more information on this system, give us a call, or leave us a message today! We are always happy to help!</p>
				</div>
				<div class="right">
					<img src="public/images/content/rosmosis.jpg" alt="" >
				</div>
		</article>
	</div>
</div>
