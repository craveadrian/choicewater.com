<div id="content">
	<div class="row">
		<article class="column">
				<h1>SERVICES</h1>
			 <p>At Choice Water Conditioning, customer satisfaction is our number one priority.We offer a number of services to ensure that your experience with Choice Water Conditioning is top-notch! From salt delivery to assistance with most older softener systems, even ones we may not sell, we go above and beyond to take care of our community.</p>




<h2>Free In-Home Estimates</h2>

<p>At Choice Water Conditioning, we are just normal members of the community just like you. We know that time is valuable and money doesn't come easy. This is why we offer free in-home estimates. Nobody likes a pushy sales pitch, so we are always happy to come out and chat with you, no strings attached. There is never any obligation for giving us a call! Contact us today and learn how you can start enjoying the countless benefits of soft, filtered water today!</p>



<h2>Free Water Testing</h2>

<p>Ever wondered just how hard your water is? Well, we can help you with that! Coupled with our free in-home estimates, we offer water testing services that will tell you down to the grain just how hard your water is. We are also able to test the chlorine content of your water as well. No matter what the results are, we have a system that works for you!</p>



<h2>Warranty Work</h2>

<p>Nobody likes to have to worry about packing, shipping, and waiting for parts to be serviced by some out of state manufacturer. At Choice Water Conditioning, we take care of that for you! If you ever have a problem, give us a call and a service person will come right out to see what we can do. We have the parts on hand for our systems, so we can fix you right up! We also provide a 1 year labor warranty on all of our installations.</p>



<h2>Optional Salt Delivery</h2>

<p>Being a customer of ours certainly has its benefits. For instance, we offer an optional salt delivery program for our customers. Whenever you need salt, just give us a call and we will bring your salt and fill your tank for you! With this service, you don't have to worry about going to the store to drag those heavy bags of salt around.</p>



<h2>Existing System Service</h2>

<p>At Choice, customer service is our number one priority. Unfortunately, that's not always the case for other companies. It is for this reason that we will work on many existing systems that we don't necessarily sell. </p>
		</article>
	</div>
</div>
